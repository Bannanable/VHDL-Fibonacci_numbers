----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Logan Suddaby
-- 
-- Create Date: 25.03.2024 10:10:41
-- Design Name: 
-- Module Name: fibonacci_tb - V1
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fibonacci_tb is
--  Port ( );
end fibonacci_tb;

architecture V1 of fibonacci_tb is
    component Fibonacci is
        Port ( clk_in : in STD_LOGIC;
               reset : in STD_LOGIC;
               Anode_activate : out STD_LOGIC_VECTOR (3 downto 0);
               LED_out : out STD_LOGIC_VECTOR (6 downto 0));
    end component;
    
    signal CLK_TB : STD_LOGIC;
    signal RESET  : STD_LOGIC;
    signal ANODE_ACTIVATE : STD_LOGIC_VECTOR (3 downto 0);
    signal LED_OUT        : STD_LOGIC_VECTOR (6 downto 0);
    
    -- Clock period
    constant T_clk : time := 10 ps;
    constant sim_clk : time := 5 * T_clk;
    constant n_cycles : integer := 10000;
begin
    
    DUT : Fibonacci
    port map(
        clk_in => CLK_TB,
        reset  => RESET,
        Anode_activate => ANODE_ACTIVATE,
        LED_out => LED_OUT);

    stimulus : process is
    begin
        reset <= '0'; wait for 2*sim_clk;
        reset <= '1'; wait for 10*sim_clk;
        reset <= '0'; wait for 100*sim_clk;
        
        wait;
    end process;
    
    -- TB clock
    clk_gen : process is
    begin
        -- Loop for n_cycles clock cycles generating a clock signal for the DUT
        while now <= (n_cycles*sim_clk) loop
            CLK_TB <= '1'; wait for T_clk/2;
            CLK_TB <= '0'; wait for T_clk/2;
        end loop;
        
        wait;
    end process;    
      
end V1;
