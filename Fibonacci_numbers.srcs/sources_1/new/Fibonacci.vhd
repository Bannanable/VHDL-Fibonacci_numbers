----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Logan Suddaby 
-- 
-- Create Date: 15.03.2024 10:14:13
-- Design Name: 
-- Module Name: Fibonacci - V1
-- Project Name: Fibonacci Sequence Generator
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- clk: Input clock signal to counter
-- reset: async reset signal to reset the counter to 0, there is a slight issue with that. The reset is technically async to the main counter but due to debouncing is synchronized to the main clock signal
-- Anode_activate: Used for controlling the seven segmented display, each bit enables a digit
-- LED_out: Controls for each individual led on the seven segmented display
entity Fibonacci is
    Port ( clk_in : in STD_LOGIC;
           reset : in STD_LOGIC;
           Anode_activate : out STD_LOGIC_VECTOR (3 downto 0);
           LED_out : out STD_LOGIC_VECTOR (6 downto 0));
end Fibonacci;

architecture V1 of Fibonacci is
    -- Constants
    constant max_count : integer := 25000000; -- Divide the clock from 100 MHz to 2 Hz
    --constant max_count : integer := 8; -- Faster operation for simulation
    constant debounce_max_count  : integer := 5000000;  -- 50 ms debounce time for a 100 MHz clock
    --constant debounce_max_count  : integer := 0; -- Disabled for simulations
    constant display_max_count : integer := 800000; -- Refresh the whole display every 8 ms
    --constant display_max_count : integer := 4; -- Faster for simulations, cant be set to less than 4
    
    -- Internal signals
    signal clk : std_logic;
    signal debounced_reset : std_logic;
    signal fibonacci : unsigned(15 downto 0) := to_unsigned(0, 16); -- Needs 1 less but as the max that can be displayed is 4 hex digits or 16^4-1 or 2^16-1, so 16 digits
    signal display_bits : unsigned(3 downto 0) := to_unsigned(0, 4); -- The value to display on the 7-segmented display digit in binary

--    component Debounce
--        Port ( button : in STD_LOGIC;
--               clk : in STD_LOGIC;
--               rst : in STD_LOGIC;
--               debounced : out STD_LOGIC);
--    end component;
begin
    -- Clock divider process
    clk_level : process (clk_in) is
        variable count : unsigned(24 downto 0) := to_unsigned(0, 25); -- Requires 25 bits to count to max_count
        variable clk_int : std_logic := '0'; -- Internal clock signal
    
    begin
        if rising_edge(clk_in) then -- When the board clock is on rising edge
            if(count < max_count) then -- If the count is still less then the max
                count := count + 1; -- Increment the count
            else
                count := to_unsigned(0, 25); -- Reset the count
                clk_int := not clk_int; -- Half of the internal clock cycle
            end if;
            
            clk <= clk_int; -- Return the internal clock signal as the Fibonacci clock
        end if;
    
    end process;    
    
    -- Input debounce process
    -- On each rising clock the button state is read
    -- If this is different to the last state then reset the timer and set the last variable
    -- If they are the same then, if the timer is long enough then the button is debounced. If not then increment the counter
    debounce : process (clk_in) is
        variable count : unsigned(22 downto 0) := to_unsigned(0, 23); -- Count to 5000000 for the debounce function
        variable last : std_logic := '0';
    begin
        if(rising_edge(clk_in)) then
            if(reset /= last) then -- If the signal has changed then reset the counter
                count := to_unsigned(0, 23);
                last := reset; -- The last state is the current state
            else -- If the button is the same
                if(count  < debounce_max_count) then -- Increment the count
                    count := count + 1;
                else -- If the count has reached the required time without change, then the button is debounced
                    -- Dont reset the timer as it will be done when the button state changes
                    debounced_reset <= reset;
                end if;
            end if;
        end if;
    end process;

    -- Fibonacci sequence generator
    -- Sensitive to the internal clock signal
    fibonacci_generator : process(clk, debounced_reset) is
        -- These two hold the current and last value
        -- Initial conditions of the sequence are 0 and 1
        variable fib0 : unsigned(16 downto 0) := to_unsigned(0, 17); -- Requires 17 digits to prevent issues when overflowing
        variable fib1 : unsigned(16 downto 0) := to_unsigned(0, 17);        
    begin
        if(debounced_reset = '1') then -- If the debounced button is high then reset the count
            -- Reset to initial conditions
            fib0 := to_unsigned(0, 17);
            fib1 := to_unsigned(1, 17); -- To stop 0 from being there twice
        elsif rising_edge(clk) then -- Only do something on rising edge
            -- Initial conditions
            if(fib0 = 0 and fib1 = 0) then
                fib1 := to_unsigned(1, 17);
            --elsif (fib0 = 0 and fib1 = 1) then
            --    fib0 := to_unsigned(1, 17);
            --    fib1 := to_unsigned(0, 17);
            else -- Main sequence time
                -- This form of the sequence is optimized for memory not runtime 
                fib0 := fib0 + fib1;
                fib1 := fib0 - fib1;
            end if;
            
            -- If a overflow was detected then reset the sequence to initial conditions
            if(fib0(16) = '1' or fib1(16) = '1') then
               fib0 := to_unsigned(0, 17);
               fib1 := to_unsigned(1, 17); -- This is set because after this call then output will be 0 then the next time it has to be changed to 1.
            end if;
        end if;
        fibonacci <= fib0(15 downto 0); -- Cutoff anybits that cant be displayed
    end process;
    
    -- 7-segmented display process
    -- Selects the required on a clock
    seven_segmented_display : process(clk_in) is
        variable count : unsigned(19 downto 0) := to_unsigned(0, 20); -- Requires 20 bits to count to 800000
    begin
        if(rising_edge(clk_in)) then
            -- First select the digit
            if(count < display_max_count) then
                -- Increment the count
                count := count + 1;
            else
                -- Reset the counter
                count := to_unsigned(0, 20);
            end if;
            
            case to_integer(count) is -- Set the current output digit
                when (display_max_count/4)*0 => 
                   Anode_activate <= "1110";
                   display_bits <= fibonacci(3 downto 0);
                when (display_max_count/4)*1 =>
                   Anode_activate <= "1101";
                   display_bits <= fibonacci(7 downto 4);
                when (display_max_count/4)*2 =>
                   Anode_activate <= "1011";
                   display_bits <= fibonacci(11 downto 8);
                when (display_max_count/4)*3 =>
                   Anode_activate <= "0111";
                   display_bits <= fibonacci(15 downto 12);
                when others =>
                   -- Do nothing, we are still displaying the last digit
            end case;
        end if;
    end process;
    
    -- Determine which LED's on the 7-segmented display to light
    -- Sensitive to the change in activated digit. Through the change in displayed char
    -- If the displayed number is the same then this wont be called as it's not needed
    seven_segmented_display_char : process(display_bits) is
    
    begin
        case to_integer(display_bits) is -- Switch for the 16 possible values
            -- LED pattern is ABC...
            -- Active low
            -- Upto 15 for hex
            when 0 =>
                LED_out <= "0000001";
            when 1 =>
                LED_out <= "1001111";
            when 2 =>
                LED_out <= "0010010";
            when 3 =>
                LED_out <= "0000110";
            when 4 =>
                LED_out <= "1001100";
            when 5 =>
                LED_out <= "0100100";
            when 6 =>
                LED_out <= "0100000";
            when 7 =>
                LED_out <= "0001111";
            when 8 =>
                LED_out <= "0000000";
            when 9 =>
                LED_out <= "0000100";
            when 10 =>
                LED_out <= "0001000";
            when 11 =>
                LED_out <= "1100000";
            when 12 =>
                LED_out <= "0110001";
            when 13 =>
                LED_out <= "1000010";
            when 14 =>
                LED_out <= "0110000";
            when 15 =>
                LED_out <= "0111000";
            when others => -- If there's an error all led off
                LED_out <= "1111111";
        end case;
    end process;
    
end V1;
