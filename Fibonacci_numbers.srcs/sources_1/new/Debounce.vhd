---------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 15.03.2024 10:14:13
-- Design Name: 
-- Module Name: Debounce - V1
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- button: The signal from the hardware button
-- clk: The clock signal require for timing
-- rst: Reset signal to reset the internal timer
-- devounced: The debounced output signal
entity Debounce is
    Port ( button : in STD_LOGIC;
           clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           debounced : out STD_LOGIC);
end Debounce;

architecture V1 of Debounce is

begin


end V1;
