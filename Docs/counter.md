# Whole system documentation

## Input
A button linked to the reset signal, this is required to be set through the debounce function.
While the debounce function isn't strictly required it impoves the accuracy of the button precesses. 

## Output
The output is in 4 digit hexadecimal counter displaying the fibonacci sequence.
When the output reaches the max value possible to be displayed on the 4 digits, it will wrap around to the inital conditions again